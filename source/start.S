.section .data
.balign 4
run_once: .word 1

.section .text.start
.arm
.global _start
.type _start, %function

_start:
    @FlushProcessDataCache
    ldr r0, =0xFFFF8001
    ldr r1, =start_addr
    ldr r2, =total_size
    svc 0x54

    ldr r0, =run_once
    mov r1, #0
    ldr r2, [r0]
    cmp r2, r1
    beq loop
    str r1, [r0]

    @ldr sp, =stack_start + 0x1000
    
    bl main
loop:
    b loop

