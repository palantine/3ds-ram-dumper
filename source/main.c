#include "3dstypes.h"
#include "3ds.h"
#include "draw.h"
#include "libc.h"
#include "crypto.h"
#include "pff.h"

static inline void sudo(void *addr)
{
	register void *_r0 asm ("r0") = addr;
	asm volatile ( "SVC 0x7B" : : "r"(_r0) );
}

void * buffer=(void *)0x01FF8000;
int size = 0x8000;
volatile char str[1024];
int j=0;
int i=0;
int armCopy() {
    for(j=0; j<1024; j++){
    	str[j]= *(char *)(buffer + j + i*1024);
    }
    return 0;
}


int main()
{
    ClearScreen();
    DEBUG("Ram Dumper by palantine"); 
    DEBUG("Opening sd:/ram.bin ...");
    FATFS fs;
    WORD w;
	FRESULT res;

	res=pf_mount(&fs);
	if(res!=FR_OK) goto fail;
	DEBUG("Mounted");
	res=pf_open("ram.bin");
	if(res!=FR_OK) goto fail;
	DEBUG("Opened! Dumping ram...");
	if(res!=FR_OK) goto fail;
	for(i=0; i<size/1024; i++){
        sudo(&armCopy);
        ClearScreen();
        DEBUG("Copying... %i", i);
        res=pf_write(&str,1024,&w);
    }
    DEBUG("Finished!");
	res = pf_write(0,0,&w);
	if(res!=FR_OK) goto fail;
	res=pf_mount(NULL);
	if(res!=FR_OK) goto fail;
	DEBUG("Success! Ram has been dumped!");
	return 0;
	fail:
		DEBUG("FAILURE %i", res);
		return 0;
    /*
	return 0;*/
}

